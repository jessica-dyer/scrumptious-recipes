from django.urls import path

from mealplans.views import (
    MealPlanListView,
    CreateMealPlan,
    MealPlanDetailView,
    UpdateMealPlan,
    DeleteMealPlan
)

urlpatterns = [
    path("meal_plans/", MealPlanListView.as_view(), name="meal_plans"),
    path("meal_plans/new/", CreateMealPlan.as_view(), name="mealplan_new"),
    path("<int:pk>/", MealPlanDetailView.as_view(), name="mealplan_detail"),
    path("<int:pk>/edit/", UpdateMealPlan.as_view(), name="mealplan_edit"),
    path("<int:pk>/delete/", DeleteMealPlan.as_view(), name="mealplan_delete"),

]
