from django.shortcuts import render

from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from mealplans.models import MealPlan


class MealPlanListView(ListView):
    model = MealPlan
    template_name = "mealplans/list.html"
    paginate_by = 10

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class CreateMealPlan(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "mealplans/new.html"
    fields = ["name", "recipes", "date"]

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("mealplan_detail", pk=plan.id)


class MealPlanDetailView(DetailView):
    model = MealPlan
    template_name = "mealplans/detail.html"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class UpdateMealPlan(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "mealplans/edit.html"
    fields = ["name", "recipes", "date"]
    success_url = reverse_lazy("meal_plans")

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

    # def get_success_url(self) -> str:
    #     return reverse_lazy("meal_plans", args=[self.object.id])


class DeleteMealPlan(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "mealplans/delete.html"
    success_url = reverse_lazy("meal_plans")

    # def get_queryset(self):
    #     return DeleteMealPlan.objects.filter(owner=self.request.user)
