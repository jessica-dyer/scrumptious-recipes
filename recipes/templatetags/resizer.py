from django import template

register = template.Library()


def resize_to(ingredient, new_serving_size):
    ingredient_amount = ingredient.amount
    original_serving_size = ingredient.recipe.servings

    try:
        if original_serving_size and new_serving_size:
            new_serving_size = int(new_serving_size)
            ingredient_amount_one_serving = (
                ingredient_amount / original_serving_size
            )
            new_amount = round(
                ingredient_amount_one_serving * new_serving_size, 1
            )
            return new_amount
    except ValueError:
        pass
    return ingredient_amount


def update_number_of_servings(original_serving_number, new_serving_number):
    # print("One: ", blah)
    # print("Two: ", arg)
    if new_serving_number is None:
        return original_serving_number
    else:
        return new_serving_number


register.filter(resize_to)
register.filter(update_number_of_servings)
