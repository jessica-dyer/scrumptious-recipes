from unicodedata import name
from django.urls import path


from recipes.views import (
    CreateRecipe,
    ShoppingItemsListView,
    UpdateRecipe,
    log_rating,
    RecipeDetailView,
    RecipeListView,
    DeleteRecipe,
    create_shopping_item,
    delete_all_shopping_items,
    signup,
)

urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("new/", CreateRecipe.as_view(), name="recipe_new"),
    path("<int:pk>/edit/", UpdateRecipe.as_view(), name="recipe_edit"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
    path("<int:pk>/delete/", DeleteRecipe.as_view(), name="recipe_delete"),
    path(
        "shopping_items/create",
        create_shopping_item,
        name="shopping_item_create",
    ),
    path(
        "shopping_items/",
        ShoppingItemsListView.as_view(),
        name="shopping_item_list",
    ),
    path(
        "shopping_items/delete",
        delete_all_shopping_items,
        name="delete_all_shopping_items",
    ),
    path("signup/", signup, name="signup"),
]
